use block_modes::{block_padding::NoPadding, BlockMode, BlockModeError, Cbc};
use blowfish::Blowfish;

use crate::errors::DeerixError;
use crate::errors::DeerixError::DecodeError;
use crate::structs::CollectedTrackInfo;

const SECRET_KEY: &[u8; 16] = b"g4el58wc0zvf9na1";
const SECRET_IV: [u8; 8] = hex_literal::hex!("0001020304050607");

pub fn decode_file(encrypted_song: &[u8], song_data: &CollectedTrackInfo) -> Result<Vec<u8>, DeerixError> {
    let hash = md5::compute(song_data.track_id.to_string()).0;
    let hash = hex::encode(hash).as_bytes().to_vec();
    let key = (0..16).fold(String::new(), |acc, i| {
        let byte = (hash[i] ^ hash[i + 16] ^ SECRET_KEY[i]) as char;
        let mut acc = acc;
        acc.push(byte);
        acc
    });

    let decrypted_song: Result<Vec<Vec<u8>>, BlockModeError> = encrypted_song
        .chunks(2048)
        .enumerate()
        .map(|(index, chunk)| {
            if index % 3 == 0 && chunk.len() == 2048 {
                let blowfish: Cbc<Blowfish, NoPadding> = Cbc::new_from_slices(key.as_bytes(), &SECRET_IV).unwrap();

                blowfish.decrypt_vec(chunk)
            } else {
                Ok(chunk.to_vec())
            }
        })
        .collect();

    let decrypted_song: Vec<_> = match decrypted_song {
        Ok(t) => t.into_iter().flatten().collect(),
        Err(e) => return Err(DecodeError(song_data.track_id, song_data.album_id, e.to_string())),
    };

    Ok(decrypted_song)
}
