use std::collections::BTreeMap;
use std::io::Write;
use std::thread::sleep;
use std::time::Duration;
use std::{fs, process};

use rand::prelude::*;
use reqwest::blocking::Client;
use serde_json::Value;
use slug::slugify;

use crate::config::{BaseConfig, Date};
use crate::errors::DeerixError::{AlbumsGetError, ArtistNotExists, ConnectionError, TrackAlbumGetError, TrackNotAvailable};
use crate::errors::{print_iteration_error, DeerixError};
use crate::structs::*;
use crate::thread_messages::thread_sleep;

pub fn get_raw_tracks_info(client: &mut Client, base_config: &BaseConfig, artist_id: u32) -> Result<(ArtistStruct, Vec<AlbumStruct>), DeerixError> {
    // println!("Started to get artist {}", &artist_id);

    let artist_info = get_artist_info(client, base_config, artist_id)?;
    // println!("Got artist info");

    let artist_albums = get_artist_albums(client, base_config, artist_id)?.data;
    // println!("Got {} albums", artist_albums.len());

    Ok((artist_info, artist_albums))
}

/// Function to get genres
pub fn _get_genre_list(client: &mut Client) {
    for i in 0..1000 {
        match client.get(format!("https://api.deezer.com/genre/{i}")).timeout(Duration::from_secs(30)).send() {
            Ok(t) => {
                let value: Value = serde_json::from_str(&t.text().unwrap()).unwrap();
                if let Some(id) = value.get("id") {
                    println!("{}i32 => {},", id, value["name"]);
                }
            }
            Err(e) => println!("{e}"),
        }
    }
}

pub fn get_track_info(client: &mut Client, base_config: &BaseConfig, track_id: u32) -> Result<FullTrackStruct, DeerixError> {
    let mut iterations_left = base_config.retrying_number;
    loop {
        let url = format!("https://api.deezer.com/track/{track_id}");
        let resp = match client.get(&url).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
            Ok(t) => t,
            Err(e) => return Err(ConnectionError(None, None, url, e.to_string())),
        };

        match resp.json::<FullTrackStruct>() {
            Ok(t) => return Ok(t),
            Err(e) => {
                if iterations_left == 0 {
                    return Err(TrackNotAvailable(track_id, e.to_string()));
                };
                print_iteration_error(e.to_string());
                iterations_left -= 1;
                thread_sleep(2.0, 8.0);
            }
        }
    }
}

/// Get info about artist
pub fn get_artist_info(client: &mut Client, base_config: &BaseConfig, artist_id: u32) -> Result<ArtistStruct, DeerixError> {
    let mut iterations_left = base_config.retrying_number;
    loop {
        let url = format!("https://api.deezer.com/artist/{artist_id}");
        let resp = match client.get(&url).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
            Ok(t) => t,
            Err(e) => return Err(ConnectionError(None, None, url, e.to_string())),
        };

        match resp.json::<ArtistStruct>() {
            Ok(t) => return Ok(t),
            Err(e) => {
                if iterations_left == 0 {
                    return Err(ArtistNotExists(artist_id, e.to_string()));
                };
                print_iteration_error(e.to_string());
                iterations_left -= 1;
                thread_sleep(2.0, 8.0);
            }
        }
    }
}

/// Download list of albums from artist
/// There is no method to list artist tracks, so probably all tracks are available 1through
pub fn get_artist_albums(client: &mut Client, base_config: &BaseConfig, artist_id: u32) -> Result<MasterAlbumStruct, DeerixError> {
    let mut iterations_left = base_config.retrying_number;
    loop {
        let url = format!("https://api.deezer.com/artist/{artist_id}/albums?limit=10000");
        let resp = match client.get(&url).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
            Ok(t) => t,
            Err(e) => return Err(ConnectionError(None, None, url, e.to_string())),
        };

        match resp.json::<MasterAlbumStruct>() {
            Ok(t) => return Ok(t),
            Err(e) => {
                if iterations_left == 0 {
                    return Err(AlbumsGetError(artist_id, e.to_string()));
                };
                print_iteration_error(e.to_string());
                iterations_left -= 1;
                thread_sleep(2.0, 8.0);
            }
        };
    }
}

pub fn get_master_in_struct_track_info(client: &mut Client, base_config: &BaseConfig, album_id: u32) -> Result<MasterTrackInStruct, DeerixError> {
    let mut iterations_left = base_config.retrying_number;
    loop {
        let album_track_url = format!("https://api.deezer.com/album/{album_id}/tracks?limit=10000");

        let resp = match client.get(&album_track_url).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
            Ok(t) => t,
            Err(e) => {
                return Err(ConnectionError(None, Some(album_id), album_track_url, e.to_string()));
            }
        };

        let text = resp.text().unwrap();
        let json_value: Value = serde_json::from_str(&text).unwrap();
        if let Some(error) = json_value.get("error") {
            let code = error["code"].as_u64().unwrap();
            if code == 800 {
                return Err(TrackAlbumGetError(
                    album_id,
                    format!("99% sure that album id not exists, try https://api.deezer.com/album/{album_id} to check this url if exists(if yes, then something strange happened)",),
                ));
            }
        }
        match serde_json::from_value::<MasterTrackInStruct>(json_value) {
            // match resp.json::<MasterTrackInStruct>() {
            Ok(t) => return Ok(t),
            Err(e) => {
                if iterations_left == 0 {
                    return Err(TrackAlbumGetError(album_id, format!("{e}")));
                };
                print_iteration_error(e.to_string());
                iterations_left -= 1;
                thread_sleep(2.0, 8.0);
            }
        };
    }
}

/// Download track info from albums
pub fn get_artist_album_tracks(
    base_config: &BaseConfig,
    client: &mut Client,
    albums: &[AlbumStruct],
    artist_data: &ArtistStruct,
    max_date: &Option<Date>,
) -> (BTreeMap<u32, CollectedTrackInfo>, Vec<DeerixError>) {
    let mut tracks: BTreeMap<u32, CollectedTrackInfo> = Default::default();
    let mut collected_errors = Vec::new();

    let filtered_albums: Vec<_> = if let Some(max_date) = max_date {
        albums
            .iter()
            .filter(|a_struct| match Date::with_string(&a_struct.release_date) {
                Ok(t) => &t >= max_date,
                Err(e) => {
                    println!("Deezer api error {} not converts to date, {e}", a_struct.release_date);
                    false
                }
            })
            .cloned()
            .collect()
    } else {
        albums.to_vec()
    };

    let thread_number = if base_config.concurrent_download == 0 {
        num_cpus::get()
    } else {
        base_config.concurrent_download
    };
    if thread_number > 4 {
        let rand_value = thread_rng().gen_range(0f32..thread_number as f32);
        sleep(Duration::from_millis((rand_value * 1000.0) as u64)); // Downloading albums one by one is really api consuming and deezer have limit 50 requests per 5 seconds
    }

    for album_struct in filtered_albums {
        let album_track_url = format!("https://api.deezer.com/album/{}/tracks?limit=10000", album_struct.id);
        let resp = match client.get(&album_track_url).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
            Ok(t) => t,
            Err(e) => {
                collected_errors.push(ConnectionError(None, Some(album_struct.id), album_track_url, e.to_string()));
                continue;
            }
        };

        let master_track = match resp.json::<MasterTrackInStruct>() {
            Ok(t) => t,
            Err(e) => {
                collected_errors.push(TrackAlbumGetError(
                    album_struct.id,
                    format!("{e} (Possible deezer Api quota exceeded - 50 requests per 5 seconds, you can try to decrease used thread number if this is too common)"),
                ));
                let rand_value = thread_rng().gen_range(0f32..5f32);
                sleep(Duration::from_millis((rand_value * 1000.0) as u64));

                continue;
            }
        };

        for track in master_track.data {
            tracks.insert(
                track.id,
                CollectedTrackInfo {
                    track_id: track.id,
                    album_id: album_struct.id,
                    title: track.title,
                    album_title: album_struct.title.clone(),
                    artist_name: artist_data.name.to_string(),
                    disk_number: track.disk_number,
                    track_position: track.track_position,
                    duration: track.duration,
                    genre: album_struct.genre_id, // Probably better would be downloading one per one
                    cover: album_struct.cover_big.clone(),
                    release_date: match Date::with_string(&album_struct.release_date) {
                        Ok(t) => Some(t),
                        Err(e) => {
                            eprintln!("Failed to convert {} to date, error {e}", album_struct.release_date);
                            None
                        }
                    },
                },
            );
        }
    }
    (tracks, collected_errors)
}

// Creates from list of artist names, list of artist_id connected to this names
pub fn search_artist_id(client: &mut Client) {
    let Ok(content) = fs::read_to_string("tosearch.txt") else {
        eprintln!("Failed to find file `tosearch.txt`");
        process::exit(1);
    };

    // Artist ids, that already are used - just put here configuration
    let already_content = match fs::read_to_string("already.txt") {
        Ok(t) => t,
        Err(_e) => String::new(),
    }
    .to_lowercase();

    let mut new_content = Vec::new();
    for line in content.split('\n') {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }

        let url = format!("https://api.deezer.com/search/artist/?q={line}");
        let resp = client.get(url).timeout(Duration::from_secs(30)).send().unwrap();
        let json: Value = serde_json::from_str(&resp.text().unwrap()).unwrap();

        let mut numbers = String::new();
        if let Some(array) = json.get("data") {
            let array = array.as_array().unwrap();
            println!("LINE - {} - {}", line, slugify(line));

            let filtered = array
                .iter()
                .filter(|e| e["type"].as_str().unwrap() == "artist")
                .filter(|e| e["name"].as_str().unwrap().to_lowercase() == line.to_lowercase())
                // .filter(|e| slugify(e["name"].as_str().unwrap()) == slugify(line))
                .map(|f| f["id"].as_i64().unwrap().to_string())
                .collect::<Vec<_>>();
            numbers = filtered.join(",");
            println!("{numbers:#?}");
        }

        if numbers.is_empty() || !already_content.contains(&format!("\n{numbers} = ")) {
            if already_content.contains(&format!("\"{}\"", line.to_lowercase())) {
                new_content.push(format!("{numbers} = \"{line}\" # Already is, look at name"));
            } else {
                new_content.push(format!("{numbers} = \"{line}\""));
            }
        } else {
            println!("Already found artist with ID {numbers} - {line}");
        }
    }
    let mut file = fs::OpenOptions::new().write(true).truncate(true).create(true).open("tosearchnew.txt").unwrap();
    write!(file, "{}", new_content.join("\n")).unwrap();
    println!("Properly searched for artists id");
}
