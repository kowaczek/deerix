use core::time::Duration;
use std::collections::BTreeMap;
use std::fs;
use std::io::{Cursor, Write};
use std::ops::Add;

use chrono::Duration as dateDuration;
use lofty::AudioFile;
use lofty::{Accessor, ItemValue, Picture, Probe, Tag, TagItem};
use lofty::{ItemKey, TaggedFileExt};
use regex::Regex;
use reqwest::blocking::Client;

use crate::config::{BaseConfig, Date};
use crate::decoding::decode_file;
use crate::deezer::deezer_generate_url;
use crate::errors::DeerixError;
use crate::errors::DeerixError::{ConnectionError, ContentTooBig, DecodeError, FileSaveError, FolderSaveError, MetadataSaveError, TrackAlbumGetError, TrackNotAvailable};
use crate::genres::get_genre;
use crate::structs::*;
use crate::thread_messages::NotDownloadedItems;

static REGEX_CACHED: once_cell::sync::Lazy<Regex> = once_cell::sync::Lazy::new(|| Regex::new(r#"[<>:"/\\|?*]"#).unwrap());

pub fn clean_file_name(file_name: &str) -> String {
    REGEX_CACHED.replace_all(file_name, "").to_string()
}

pub fn filter_artists_by_time(map: BTreeMap<u32, (Option<Date>, String)>, force_download: bool, days: u32) -> BTreeMap<u32, (Option<Date>, String)> {
    if force_download {
        return map;
    }
    let previous_size = map.len();
    let new_map: BTreeMap<u32, (Option<Date>, String)> = map
        .into_iter()
        .filter(|(_artist_id, (max_date, _comment))| {
            if let Some(max_date) = max_date.clone() {
                if let Some(valid_date) = chrono::NaiveDate::from_ymd_opt(max_date.year as i32, max_date.month, max_date.day) {
                    let current_date = chrono::offset::Utc::now();
                    if valid_date.add(dateDuration::days(days as i64)) > current_date.date_naive() {
                        return false;
                    }
                }
            }
            true
        })
        .collect();
    let new_size = new_map.len();
    println!(
        "Ignored {} out of all {previous_size} artists were downloaded in last {days} days.",
        previous_size - new_size
    );
    new_map
}

pub fn download_and_save_song(client: &mut Client, base_config: &BaseConfig, song_data: &CollectedTrackInfo, url: &str, extension: &str) -> Result<(), DeerixError> {
    let response = match client.get(url).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
        Ok(t) => t,
        Err(e) => return Err(ConnectionError(Some(song_data.track_id), Some(song_data.album_id), url.to_string(), e.to_string())),
    };

    if let Some(length) = response.content_length() {
        if length > 1024 * 1024 * base_config.maximum_song_size_mb as u64 {
            return Err(ContentTooBig(song_data.track_id, song_data.album_id, length));
        }
    }

    let data = match response.bytes() {
        Ok(t) => t,
        Err(e) => return Err(ConnectionError(Some(song_data.track_id), Some(song_data.album_id), url.to_string(), e.to_string())),
    };

    let downloaded_song = decode_file(&data, song_data)?;

    let cover = if let Some(cover_url) = &song_data.cover {
        let mut iterations_left = base_config.retrying_number;
        loop {
            match client.get(cover_url).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
                Ok(t) => {
                    if let Ok(resp) = t.bytes() {
                        break Some(resp.to_vec());
                    }
                    break None;
                }
                Err(e) => {
                    if iterations_left == 0 {
                        break None;
                    }
                    iterations_left -= 1;
                    eprintln!("Failed to download picture image {e}");
                    continue;
                }
            }
        }
    } else {
        None
    };

    let save_folder = format!(
        "{}/{}/{}",
        base_config.download_path,
        clean_file_name(&song_data.artist_name),
        clean_file_name(&song_data.album_title)
    );

    if let Err(e) = fs::create_dir_all(&save_folder) {
        return Err(FolderSaveError(song_data.track_id, song_data.album_id, save_folder, e.to_string()));
    }

    let save_path = format!("{}/{}.{extension}", save_folder, clean_file_name(&song_data.title));
    save_metadata(&downloaded_song, song_data, &save_path, cover)
}

pub fn save_metadata(downloaded_song: &[u8], song_data: &CollectedTrackInfo, save_path: &str, cover: Option<Vec<u8>>) -> Result<(), DeerixError> {
    let mut file_handler = match fs::OpenOptions::new().write(true).create(true).truncate(true).open(save_path) {
        Ok(t) => t,
        Err(e) => return Err(FileSaveError(song_data.track_id, song_data.album_id, save_path.to_string(), e.to_string())),
    };

    if let Err(e) = file_handler.write_all(downloaded_song) {
        return Err(FileSaveError(song_data.track_id, song_data.album_id, save_path.to_string(), e.to_string()));
    }

    let mut tagged_file = match Probe::new(Cursor::new(&downloaded_song)).guess_file_type() {
        Ok(t) => match t.read() {
            Ok(t) => t,
            Err(e) => {
                return Err(MetadataSaveError(song_data.track_id, song_data.album_id, save_path.to_string(), e.to_string()));
            }
        },
        Err(e) => {
            return Err(MetadataSaveError(song_data.track_id, song_data.album_id, save_path.to_string(), e.to_string()));
        }
    };

    let tag = match tagged_file.primary_tag_mut() {
        Some(primary_tag) => primary_tag,
        None => {
            if let Some(first_tag) = tagged_file.first_tag_mut() {
                first_tag
            } else {
                let tag_type = tagged_file.primary_tag_type();
                tagged_file.insert_tag(Tag::new(tag_type));

                tagged_file.primary_tag_mut().unwrap()
            }
        }
    };

    tag.set_title(song_data.title.clone());
    tag.set_album(song_data.album_title.clone());
    tag.set_artist(song_data.artist_name.clone());
    tag.set_disk(song_data.disk_number);
    tag.set_track(song_data.track_position);
    if let Some(genre) = get_genre(song_data.genre) {
        tag.set_genre((*genre).to_string());
    }
    if let Some(date) = &song_data.release_date {
        tag.set_year(date.year);
        tag.insert(TagItem::new(ItemKey::Year, ItemValue::Text(date.year.to_string())));
        // TODO not sure why recording date is not set
    }

    if let Some(cover) = cover {
        match Picture::from_reader(&mut Cursor::new(cover)) {
            Ok(valid_image) => {
                tag.push_picture(valid_image);
            }
            Err(e) => {
                println!("Failed to decode image, error {e}");
            }
        }
    }

    match tagged_file.save_to_path(save_path) {
        Ok(t) => t,
        Err(e) => return Err(FileSaveError(song_data.track_id, song_data.album_id, save_path.to_string(), e.to_string())),
    };

    Ok(())
}

pub fn collect_song_data_from_collected_track(full_track_struct: &FullTrackStruct) -> CollectedTrackInfo {
    CollectedTrackInfo {
        track_id: full_track_struct.id,
        album_id: full_track_struct.album.id,
        title: full_track_struct.title.clone(),
        album_title: full_track_struct.album.title.clone(),
        artist_name: full_track_struct.artist.name.to_string(),
        disk_number: full_track_struct.disk_number,
        track_position: full_track_struct.track_position,
        duration: full_track_struct.duration,
        genre: -1, // Not sure why this is not available, probably second check for artist should be get
        cover: full_track_struct.album.cover_big.clone(),
        release_date: match Date::with_string(&full_track_struct.album.release_date) {
            Ok(t) => Some(t),
            Err(e) => {
                eprintln!("Failed to convert {} to date, error {e}", full_track_struct.album.release_date);
                None
            }
        },
    }
}

pub fn generate_url_and_download_it(track_id: u32, song_data: &CollectedTrackInfo, base_config: &BaseConfig, client: &mut Client) -> Result<(), DeerixError> {
    let url_data = match deezer_generate_url(client, base_config, track_id) {
        Ok(t) => t,
        Err(e) => {
            eprintln!("{e}");
            return Err(e);
        }
    };

    for (url_index, (url, extension)) in url_data.iter().enumerate() {
        if let Err(e) = download_and_save_song(client, base_config, song_data, url, extension) {
            eprintln!("{e}");
            if url_index + 1 == url_data.len() {
                return Err(e);
            }
        } else {
            return Ok(());
        }
    }

    Err(TrackNotAvailable(track_id, format!("Cannot find urls for this track with id {track_id}")))
}

pub fn get_not_downloaded_songs_from_errors(errors: Vec<DeerixError>) -> NotDownloadedItems {
    let mut not_downloaded = NotDownloadedItems::default();

    for e in errors {
        match e {
            ConnectionError(track_id, album_id, _, _) => {
                if let Some(track_id) = track_id {
                    not_downloaded.songs.push((track_id, e.to_string()));
                } else if let Some(album_id) = album_id {
                    not_downloaded.albums.push((album_id, e.to_string()));
                } else {
                    panic!("Connection error without track or album id")
                }
            }
            TrackAlbumGetError(album_id, _) => {
                not_downloaded.albums.push((album_id, e.to_string()));
            }
            DecodeError(track_id, _, _) | FolderSaveError(track_id, _, _, _) | FileSaveError(track_id, _, _, _) | MetadataSaveError(track_id, _, _, _) => {
                not_downloaded.songs.push((track_id, e.to_string()));
            }
            ContentTooBig(track_id, _, _) | TrackNotAvailable(track_id, _) => {
                not_downloaded.ignored_songs.push((track_id, e.to_string()));
            }
            _ => panic!("Error {e} should be handled, but is not"),
        }
    }
    not_downloaded
}
