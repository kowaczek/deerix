use serde::Deserialize;

use crate::config::Date;

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct UserStruct {
    pub(crate) id: u32,
    pub(crate) name: String,
    pub(crate) link: String,
    pub(crate) picture: String,
    pub(crate) picture_small: String,
    pub(crate) picture_medium: String,
    pub(crate) picture_big: String,
    pub(crate) picture_xl: String,
    pub(crate) country: String,
    pub(crate) tracklist: String,
    pub(crate) r#type: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct ArtistStruct {
    pub(crate) id: u32,
    pub(crate) name: String,
    pub(crate) link: String,
    pub(crate) share: String,
    pub(crate) picture: String,
    pub(crate) picture_small: String,
    pub(crate) picture_medium: String,
    pub(crate) picture_big: String,
    pub(crate) picture_xl: String,
    pub(crate) nb_album: u32,
    pub(crate) nb_fan: u32,
    pub(crate) radio: bool,
    pub(crate) tracklist: String,
    pub(crate) r#type: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct AlbumStruct {
    pub(crate) id: u32,
    pub(crate) title: String,
    pub(crate) link: String,
    pub(crate) cover: String,
    pub(crate) cover_small: Option<String>,
    pub(crate) cover_medium: Option<String>,
    pub(crate) cover_big: Option<String>,
    pub(crate) cover_xl: Option<String>,
    pub(crate) md5_image: String,
    pub(crate) genre_id: i32,
    pub(crate) fans: u32,
    pub(crate) release_date: String,
    pub(crate) record_type: String,
    pub(crate) tracklist: String,
    pub(crate) explicit_lyrics: bool,
    pub(crate) r#type: String,
    pub(crate) tracks: Option<String>,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct MasterAlbumStruct {
    pub(crate) data: Vec<AlbumStruct>,
    pub(crate) total: u32,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct MasterTrackInStruct {
    pub(crate) data: Vec<TrackStructInAlbums>,
    pub(crate) total: u32,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct TrackStructInAlbums {
    pub(crate) id: u32,
    pub(crate) readable: bool,
    pub(crate) title: String,
    pub(crate) title_short: String,
    pub(crate) title_version: Option<String>,
    pub(crate) isrc: String,
    pub(crate) link: String,
    pub(crate) duration: u32,
    pub(crate) track_position: u32,
    pub(crate) disk_number: u32,
    pub(crate) rank: u32,
    pub(crate) explicit_lyrics: bool,
    pub(crate) explicit_content_lyrics: u32,
    pub(crate) explicit_content_cover: u32,
    pub(crate) preview: String,
    pub(crate) md5_image: String,
    pub(crate) artist: ArtistInTrackStruct,
    pub(crate) r#type: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct ArtistInTrackStruct {
    pub(crate) id: u32,
    pub(crate) name: String,
    pub(crate) tracklist: String,
    pub(crate) r#type: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct TrackStruct {
    pub(crate) id: u32,
    pub(crate) readable: bool,
    pub(crate) title: String,
    pub(crate) title_short: String,
    pub(crate) title_version: Option<String>,
    pub(crate) isrc: String,
    pub(crate) link: String,
    pub(crate) share: String,
    pub(crate) duration: u32,
    pub(crate) track_position: u32,
    pub(crate) disk_number: u32,
    pub(crate) rank: u32,
    pub(crate) release_date: String,
    pub(crate) explicit_lyrics: bool,
    pub(crate) explicit_content_lyrics: u32,
    pub(crate) explicit_content_cover: u32,
    pub(crate) preview: String,
    pub(crate) bpm: f32,
    pub(crate) gain: f32,
    pub(crate) available_countries: Vec<String>,
    pub(crate) r#type: String,
}

// TRACK START
#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct AlbumInFullTrackStruct {
    pub(crate) id: u32,
    pub(crate) title: String,
    pub(crate) link: String,
    pub(crate) cover: String,
    pub(crate) cover_small: Option<String>,
    pub(crate) cover_medium: Option<String>,
    pub(crate) cover_big: Option<String>,
    pub(crate) cover_xl: Option<String>,
    pub(crate) md5_image: String,
    pub(crate) release_date: String,
    pub(crate) tracklist: String,
    pub(crate) r#type: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct ArtistInFullTrackStruct {
    pub(crate) id: u32,
    pub(crate) name: String,
    pub(crate) link: String,
    pub(crate) share: String,
    pub(crate) picture: String,
    pub(crate) picture_small: String,
    pub(crate) picture_medium: String,
    pub(crate) picture_big: String,
    pub(crate) picture_xl: String,
    pub(crate) radio: bool,
    pub(crate) tracklist: String,
    pub(crate) r#type: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct FullTrackStruct {
    pub(crate) id: u32,
    pub(crate) readable: bool,
    pub(crate) title: String,
    pub(crate) title_short: String,
    pub(crate) title_version: Option<String>,
    pub(crate) isrc: String,
    pub(crate) link: String,
    pub(crate) share: String,
    pub(crate) duration: u32,
    pub(crate) track_position: u32,
    pub(crate) disk_number: u32,
    pub(crate) rank: u32,
    pub(crate) release_date: String,
    pub(crate) explicit_lyrics: bool,
    pub(crate) explicit_content_lyrics: u32,
    pub(crate) explicit_content_cover: u32,
    pub(crate) preview: String,
    pub(crate) bpm: f32,
    pub(crate) gain: f32,
    pub(crate) available_countries: Vec<String>,
    pub(crate) r#type: String,
    pub(crate) artist: ArtistInFullTrackStruct,
    pub(crate) album: AlbumInFullTrackStruct,
}
// TRACK STOP

#[allow(dead_code)]
#[derive(Debug)]
pub struct CollectedTrackInfo {
    pub track_id: u32,
    pub album_id: u32,
    pub title: String,
    pub album_title: String,
    pub artist_name: String,
    pub disk_number: u32,
    pub track_position: u32,
    pub duration: u32,
    pub genre: i32,
    pub cover: Option<String>,
    pub release_date: Option<Date>,
}
